# Gitlab Runner

Multi-phase process for standing up/configuring a gitlab-runner for gitlab


## Phase 1: Build the runner image
We will build the runner image using packer and ansible to configure the runner image.
This process will be done on the command line.

## Phase 2: Launch the runner
We will build an instance of the runner image and launch it.
Terraform will be used to launch the instance.

## Phase 3: Use the runner
Once the runner is established, we will use that runner to build the gitlab-runner image going forward

## TODO 
1. Setup the ci/cd pipeline
2. modify the runner to use the gitlab-runner image
3. update the runner with the new image
4. automate the update of the runner-image via gitlab-ci/cd
