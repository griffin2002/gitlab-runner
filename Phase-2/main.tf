data "aws_ami" "gitlab-runner" {
  most_recent = true
  filter {
    name   = "gitlab-runner"
    values = ["gitlab-runner*"]
  }
  filter {
    name   = "virutalization-type"
    values = ["hvm"]
  }
  owners = [var.vpc_id]
}

resource "aws_instance" "gitlab-runner" {
  ami           = data.aws_ami.gitlab-runner.id
  instance_type = "t3.medium"

  tags = {
    Name = "gitlab-runner"
  }
}