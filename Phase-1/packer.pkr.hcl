packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "gitlab-runner" {

  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
  vpc_id     = var.vpc_id
  subnet_id  = var.subnet_id
  source_ami_filter {
    filter {
      name             = "ubuntu"
      values           = ["amzn-ami-hvm-*"]
      root-device-type = "ebs"
    }
    most_recent = true
  }
  instance_type   = var.instance_type
  ssh_username    = "ubuntu"
  ami_name        = "gitlab-runner-ami"
  ami_description = "GitLab Runner AMI"
  tags {
    Name        = "gitlab-runner-ami"
    Builder     = "Packer"
    Provisioner = "Ansible"
  }
}

build {
  sources = ["source.amazon-ebs.gitlab-runner"]

  provisioner "shell" {
    execute_command = "echo 'ec2-user' | {{.Vars}} sudo -S -E bash '{{.Path}}'"
    inline          = ["apt-get update -y"]

  }
  provisioner "ansible" {
    playbook = "./gitlab-runner.yml"
  }
  post-processor "manifest" {
    output     = "gitlab-runner.json"
    strip_path = true
  }
}


variable "access_key" {
  default = "{{user `aws_access_key`}}"
}
variable "secret_key" {
  default = "{{user `aws_secret_key`}}"
}
variable "region" {
  default = "us-east-1"
}
variable "vpc_id" {
  default = "{{user `aws_vpc_id`}}"
}
variable "subnet_id" {
  default = "{{user `aws_subnet_id`}}"
}
variable "instance_type" {
  default = "t2.medium"
}
